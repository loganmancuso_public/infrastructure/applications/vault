##############################################################################
#
# Author: Logan Mancuso
# Created: 07.30.2023
#
##############################################################################

terraform {
  required_version = ">= 1.6.0"
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = ">=3.0.2"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}