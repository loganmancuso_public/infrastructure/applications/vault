##############################################################################
#
# Author: Logan Mancuso
# Created: 11.27.2023
#
##############################################################################

locals {
  app_root        = "${path.module}/app"
  app_config_path = "${local.app_root}/config"
  app_certs_path  = "${local.app_root}/certs"
  app_data_path   = "${local.app_root}/data"
  app_name        = "vault"
}

# Template Vault Config File #
data "template_file" "vault_config" {
  depends_on = [terraform_data.application_certs]
  template   = file("${path.module}/templates/main.hcl")
  vars = {
    tls_cert_file = basename(local_file.server_vault["ca"].filename)
    tls_key_file  = basename(local_file.server_vault["key_private"].filename)
  }
}

resource "local_file" "vault_config_rendered" {
  content  = data.template_file.vault_config.rendered
  filename = "${local.app_config_path}/config.hcl"
}

# Create a network for the app
resource "docker_network" "vault" {
  name = "network-${local.app_name}"
}

# Pull the vault image
resource "docker_image" "vault" {
  name = "hashicorp/vault:latest"
}

# Create the vault container
resource "docker_container" "vault" {
  name    = "app-${local.app_name}"
  image   = docker_image.vault.image_id
  restart = "always"
  env = [
    "TZ=America/New_York"
  ]
  volumes {
    volume_name    = "${abspath(path.module)}/${local.app_data_path}" # docker_volume.vault.name
    container_path = "/mnt/vault-data"
    read_only      = false
  }
  volumes {
    host_path      = "${abspath(path.module)}/${local.app_config_path}"
    container_path = "/vault/config"
    read_only      = true
  }
  volumes {
    host_path      = "${abspath(path.module)}/${local.app_certs_path}"
    container_path = "/etc/ssl/certs"
    read_only      = true
  }
  network_mode = "bridge"
  networks_advanced {
    name = docker_network.vault.name
  }
  ports {
    internal = 8200
    external = 8200
  }
  ports {
    internal = 8201
    external = 8201
  }
  entrypoint = ["vault", "server", "-config", "/vault/config"]
  depends_on = [terraform_data.application_certs, local_file.vault_config_rendered]
}
