##############################################################################
#
# Author: Logan Mancuso
# Created: 11.27.2023
#
##############################################################################

variable "cert_subject" {
  description = "subject for certificate, this is generated to secure the hashi vault in a prod environment"
  type = object({
    common_name  = string
    organization = string
    country      = string
    locality     = string
    postal_code  = string
    province     = string
  })
}