##############################################################################
#
# Author: Logan Mancuso
# Created: 11.27.2023
#
##############################################################################

output "application_details" {
  description = "application details"
  value = {
    app = {
      name       = docker_container.vault.name
      cpu_shares = docker_container.vault.cpu_shares
      hostname   = docker_container.vault.hostname
    }
    network_name = docker_network.vault.name
  }
}

## Certificates ##
output "cert_root" {
  description = "root certificate and keys, to store in vault for other services"
  sensitive   = true
  value = {
    cert    = tls_self_signed_cert.root
    keys    = tls_private_key.root
    subject = var.cert_subject
  }
}