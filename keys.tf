##############################################################################
#
# Author: Logan Mancuso
# Created: 11.27.2023
#
##############################################################################

## ROOT CA ##
# root keypair, used to sign the root Cert
resource "tls_private_key" "root" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
# this is the Root Cert used to sign server_vault Certs 
resource "tls_self_signed_cert" "root" {
  private_key_pem      = tls_private_key.root.private_key_pem
  is_ca_certificate    = true
  set_authority_key_id = true
  set_subject_key_id   = true
  subject {
    common_name  = "${var.cert_subject.common_name} Root"
    organization = var.cert_subject.organization
    country      = var.cert_subject.country
    locality     = var.cert_subject.locality
    postal_code  = var.cert_subject.postal_code
    province     = var.cert_subject.province
  }

  validity_period_hours = 87660 # 10 years
  early_renewal_hours   = 365

  allowed_uses = [
    "any_extended",       # Any extended usage allowed
    "cert_signing",       # Permission to sign certificates
    "client_auth",        # Permission for client authentication
    "code_signing",       # Permission for code signing
    "content_commitment", # Permission for content commitment
    "crl_signing",        # Permission to sign Certificate Revocation Lists (CRLs)
    "data_encipherment",  # Permission for data encipherment
    "decipher_only",      # Permission for decipher only
    "digital_signature",  # Permission for digital signature
    "email_protection",   # Permission for email protection
    "encipher_only",      # Permission for encipher only
    "ipsec_end_system",   # Permission for IPsec end system
    "ipsec_tunnel",       # Permission for IPsec tunnel
    "ipsec_user",         # Permission for IPsec user
    "key_agreement",      # Permission for key agreement
    "key_encipherment",   # Permission for key encipherment
    "ocsp_signing",       # Permission for OCSP (Online Certificate Status Protocol) signing
    "server_auth",        # Permission for server authentication
    "timestamping"        # Permission for timestamping
  ]
}
# Local Cert Files 
locals {
  root = {
    key_private = tls_private_key.root.private_key_pem
    key_public  = tls_private_key.root.public_key_pem
    ca          = tls_self_signed_cert.root.cert_pem
  }
}
resource "local_file" "root" {
  for_each = local.root
  content  = each.value
  filename = "${path.module}/keys/root/${each.key}.pem"
}

## server_vault CA ##
# Request Cert from Root CA
# Generate the CSR (Certificate Signing Request) for the server_vault CA
resource "tls_private_key" "server_vault" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "tls_cert_request" "server_vault" {
  private_key_pem = tls_private_key.server_vault.private_key_pem
  dns_names       = ["localhost"]
  subject {
    common_name  = "${var.cert_subject.common_name} server_vault"
    organization = var.cert_subject.organization
    country      = var.cert_subject.country
    locality     = var.cert_subject.locality
    postal_code  = var.cert_subject.postal_code
    province     = var.cert_subject.province
  }
}
# server_vault Cert
# using the request generate the server_vault cert
resource "tls_locally_signed_cert" "server_vault" {
  cert_request_pem      = tls_cert_request.server_vault.cert_request_pem
  ca_private_key_pem    = tls_private_key.root.private_key_pem
  ca_cert_pem           = tls_self_signed_cert.root.cert_pem
  set_subject_key_id    = true
  validity_period_hours = 87660 # 10 years
  early_renewal_hours   = 365

  allowed_uses = [
    "key_encipherment",  # Permission for key encipherment
    "digital_signature", # Permission for digital signature
    "cert_signing",      # Permission to sign certificates
    "crl_signing",       # Permission to sign Certificate Revocation Lists (CRLs)
    "client_auth",       # Permission for client authentication
    "server_auth"        # Permission for server authentication
  ]
}
# Local Cert Files 
locals {
  server_vault = {
    key_private = tls_private_key.server_vault.private_key_pem
    key_public  = tls_private_key.server_vault.public_key_pem
    ca          = tls_locally_signed_cert.server_vault.cert_pem
  }
}
resource "local_file" "server_vault" {
  for_each = local.server_vault
  content  = each.value
  filename = "${path.module}/keys/server_vault/${each.key}.pem"
}

resource "terraform_data" "application_certs" {
  depends_on       = [local_file.server_vault, local_file.root]
  triggers_replace = [tls_self_signed_cert.root, tls_locally_signed_cert.server_vault]
  input = {
    app_root = local.app_root
  }
  provisioner "local-exec" {
    when        = create
    working_dir = path.module
    command = join(" && ", [
      # remove docker app directory if created
      "[ -d \"${local.app_root}\" ] && rm -r \"${local.app_root}\"; mkdir -p \"${local.app_root}\"",
      "[ -d \"${local.app_config_path}\" ] && rm -r \"${local.app_config_path}\"; mkdir -p \"${local.app_config_path}\"",
      "[ -d \"${local.app_certs_path}\" ] && rm -r \"${local.app_certs_path}\"; mkdir -p \"${local.app_certs_path}\"",
      # copy key to docker container mount
      "cp ${local_file.server_vault["ca"].filename} ${local.app_certs_path}/ca.pem",
      "cp ${local_file.server_vault["key_private"].filename} ${local.app_certs_path}/key_private.pem",
      # compress keys for backup
      "tar -czvf ${path.module}/keys/keys.tar.gz ${path.module}/keys/server_vault ${path.module}/keys/root",
      # add root cert to local system
      "sudo cp ${local_file.root["ca"].filename} /etc/ssl/certs/${var.cert_subject.common_name}.pem",
      "sudo update-ca-certificates"
    ])
  }
  provisioner "local-exec" {
    when        = destroy
    working_dir = path.module
    command = join(" && ", [
      "rm -r ${self.input.app_root}",
      "rm -r ${path.module}/keys"
    ])
  }
}
